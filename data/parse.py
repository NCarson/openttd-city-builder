import sys

import pandas as pd

#csv format:
#City,State,Type,Counties,Population,Latitude,Longitude
#New York,NY,City,Bronx;Richmond;New York;Kings;Queens,8804190,40.714,-74.007

# setup data frame
df = pd.read_csv("us2021census.csv")
df = df[df.State == 'OR']
#XXX make sure highest population is first as squirrel will expect that
df = df.sort_values(by="Population", ascending=False)

# squirrel array format
# state, name, pop, lat, lng, elevation, by_river, by_sea, bin_rank

# write squirrel data file
out = ['function GetCities() { return [']
for idx, row in df.iterrows():
    line = '    ["{}", "{}", {}, {}, {}, null, null, null, null],'.format(
        row.State, row.City, row.Population,
        round(row.Latitude,3), round(row.Longitude,3),
        )
    out.append(line)
out.append("]}")

sys.stdout.write('\n'.join(out))
sys.stdout.write('\n')

'''

bins = [0, .25, .5, .7, .8, .95, .999, 1]
labels = range(len(bins) - 1)
df['bin'] = pd.qcut(df['Population'], bins, labels=labels)

import math
def distance(lat1, lng1, lat2, lng2):
    return math.sqrt((lat1 - lat2)**2 + (lng1 - lng2)**2)

row = df[df.City=='Coos Bay'].iloc[0]
lat, lng = row.Latitude, row.Longitude
ARCMIN = 1/60.
for idx, row in df.iterrows():
    if distance(row.Latitude, row.Longitude, lat, lng) < ARCMIN * 100:
        print(11, row.City)

'''
