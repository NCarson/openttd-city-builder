import math

class GPSToXY:
    def __init__(self, gps_coords, map_size):
        # Set up bounding box to hold data member
        self.bounding_box = self.get_bounding_box(gps_coords)
        # Set up map size to hold data member
        self.map_size = map_size
        
    def get_bounding_box(self, gps_coords):
        # Get minimum and maximum latitudes and longitudes
        min_lat = min(coord[0] for coord in gps_coords)
        max_lat = max(coord[0] for coord in gps_coords)
        min_lon = min(coord[1] for coord in gps_coords)
        max_lon = max(coord[1] for coord in gps_coords)
        # Return bounding box as a tuple of (min_lat, max_lat, min_lon, max_lon)
        return (min_lat, max_lat, min_lon, max_lon)
        
    def toXY(self, gps_coord):
        # Check if GPS coordinate is within bounding box
        if gps_coord[0] < self.bounding_box[0] or gps_coord[0] > self.bounding_box[1] or gps_coord[1] < self.bounding_box[2] or gps_coord[1] > self.bounding_box[3]:
            return None
        
        # Convert GPS coordinates to x, y coordinates using the IV quadrant in relation to the origin
        lat_diff = gps_coord[0] - self.bounding_box[0]
        lon_diff = gps_coord[1] - self.bounding_box[2]
        x = lon_diff * math.cos(math.radians(self.bounding_box[0]))
        y = lat_diff
        
        # Scale x and y coordinates to map size and round to nearest integer tile index
        x = round((x / (self.bounding_box[3] - self.bounding_box[2])) * (self.map_size - 1))
        y = round((y / (self.bounding_box[1] - self.bounding_box[0])) * (self.map_size - 1))
        y = SIZE - y
        
        return (x, y)


if __name__ == '__main__':

    import pandas as pd
    df = pd.read_csv("us2021census.csv")
    df.sort_values(by='Population', ascending=False)
    df = df[df.State == 'OR']

    SIZE = 80
    coords = []
    for i, (idx, row) in enumerate(df.iterrows()):
        coords.append((row.Latitude, row.Longitude, row.City))
    
    map_ = GPSToXY(coords, SIZE)
    out = '' 
    for y in range(SIZE):
        for x in range(SIZE):
            found = False
            for lat, lng, city in coords[:10]:
                xx, yy = map_.toXY((lat, lng))
                if xx == x and yy == y:
                    found = True
                    c = city
            if found:
                print(x, y, c)
                out += 'X'
            else:
                out += '.'
        out += '\n'

print(out)
                    



