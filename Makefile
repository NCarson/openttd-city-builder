GITLAB_USER = NCarson
GITLAB_PROJECT = openttd-city-builder

GITLAB_SSH = git@gitlab.com:$(GITLAB_USER)/$(GITLAB_PROJECT).git

.PHONY: push-new
push-new:
	git remote add origin $(GITLAB_SSH)
	git push -u origin --all
	git push -u origin --tags

.PHONY: push
push: anoncommit
	git push origin master

# this will need a fixme and clean recipe in the implemented files
.PHONY: anoncommit
anoncommit:
	$(MAKE) clean
	$(MAKE) fixme
	git add . &&  git commit -m ...

.PHONY: clean
clean:
	true

.PHONY: fixme
fixme:
	true


