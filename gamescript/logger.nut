
class Logger
{
	levels = ["DEBUG", "INFO", "WARN", "ERROR"]
	DEBUG = 0
	INFO = 1
	WARN = 2
	ERROR = 3
	name = null

	constructor(name){
		this.name = name	
	}

	function Debug(method, msg) { this.Log(this.DEBUG, method, msg) }
	function Info(method, msg) { this.Log(this.INFO, method, msg) }
	function Warn(method, msg) { this.Log(this.WARN, method, msg) }
	function Error(method, msg) { this.Log(this.ERROR, method, msg) }

	function Log(level, method, msg) {

		local method = this.name + "." + method + "() : " 
		local lname = null
		if (level > 4 || level < 0) {
			throw "invalid log level of " + level
		} else {
			lname = levels[level]
		}
		GSLog.Info(lname + " : " + method + msg)
	}

	function Test() {
		class GSLog 
		{
			static function Info(msg) { print(msg + "\n") }
			static function Warning(msg) { print(msg + "\n") }
			static function Error(msg) { print(msg + "\n") }
		}
		local msg = "this is a test"
		local logger = Logger("TestLog")
		logger.Log(5, "Test", msg)
		logger.Log(Logger.DEBUG, "Test", msg)
	}
}

//Logger.Test()


