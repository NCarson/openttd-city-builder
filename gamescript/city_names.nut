dofile("CoreUtils.nut") // we need a copy of core utils for command line access
dofile("custom_quantile.nut")

pprint <- CoreUtils.pprint
round <- CoreUtils.Math.round
Array <- CoreUtils.Array
Rand <- CoreUtils.Rand

class CityNames {

	// cities follow an exponentional distribution
	static BIN_CUTOFFS = [0, 0.25, 0.5, 0.75, 0.8, 0.995, 1]

	function _PickNamesInner(data, rank, used, out) {
		local found = false
		foreach (idx, row in data) {
			if (row[0] == rank && !Array.contains(used, idx)) {
				out.append(row[1])
				found = true
				used.append(idx)
				break
			}
		}
		return found
	}
	
	function PickNames(selection, map_bins) {

		// data will be in form of [ [bin rank, name], ... ] 
		local data = this._ShuffleByBins(this._RankCities(selection))
		local out = []
		local used = []

		foreach (rank in map_bins) {
			local r = rank
			// There may be not enough cities in a rank to fill all the slots.
			// while we failed at picking at city keep trying lower ranks until we run out
			while(!this._PickNamesInner(data, r, used, out)) {
				r = r - 1
				if (r == 0) {
					throw "not enough cities with a bin rank of " + rank.tostring() + " (widen selection criteria)"
				}
			}
		}
		return out
	}

	function _RankCities(selection, cutoffs=null) { //FIXME we need to enforce that the selection is sorted correctly

		if (cutoffs == null)
			cutoffs = this.BIN_CUTOFFS.slice(0) // make sure we dont hold ref

		local pop = selection.ByColumn(selection.KEYS.POPULATION)
		pop.reverse() // has to be in ascending order
		local names = selection.ByColumn(selection.KEYS.NAME)
		local bins = CustomQuantile(pop, cutoffs)
		bins.reverse()

		if (pop.len() != bins.len())
			throw "population and bins are different sizes"

		local out = []
		foreach (idx, name in names) {
			out.append([bins[idx], name])
		}
		return out
	}

	function _ShuffleByBins(data) {
		local out = []
		local bins = Array.unique(Array.column(data, 0)) // get unique ranks
		foreach(rank in bins) {
			local tmp = Array.select(data, 0, rank) // select the rows with the current rank
			Rand.shuffle(tmp) // shuffle the order of the cities *within* rank
			out.extend(tmp)
		}
		return out
	}


	static function _test() {

		local map_bins = [6,6,5,2,2,1] // mock bins from openttd map
		local data = CityData()
		local selection = data.Select()
		local names = CityNames()
		pprint(names.PickNames(selection, map_bins))
	}
}
//dofile("city_data.nut"); CityNames._test()
