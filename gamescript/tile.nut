try {
	require("util.nut")
} catch(err) { // for testing (require does not exist outside of openttd)
	dofile("util.nut")
}

class Direc { // isometric (N looks like NW) -- rotate clockwise 45deg for birds eye direction
	static N  = [ 0, -1]
	static S  = [ 0,  1]
	static W  = [ 1,  0]
	static E  = [-1,  0]
	static NW = [ 1,  -1]
	static NE = [-1,  -1]
	static SW = [ 1,   1]
	static SE = [-1,   1]

	static function Add(x, y, direc) {
		return [x + direc[0], y + direc[1]]
	}
}
Direc.Cardinal = [
	Direc.N, Direc.S, Direc.W, Direc.E,
]
Direc.Ordinal = [
	Direc.NW, Direc.NE, Direc.SW, Direc.SE,
]
Direc.All = [
	Direc.N, Direc.S, Direc.W, Direc.E,
	Direc.NW, Direc.NE, Direc.SW, Direc.SE,
]

class Tile {

	this.tileid = null
	this.x = null
	this.y = null

	constructor(tileid) {
		
		if (!GSMap.IsValidTile(tileid))
			throw "tile " + tielid + " is not a valid tile"
		this.tileid = tileid
		this.x = GSMap.GetTileX(this.tileid)
		this.y = GSMap.GetTileY(this.tileid)
	}

	_tostring() {
		return "Tile(" + this.x + "," + this.y + ")"
	}
	
	function Neighbor(direc) {
		local xy = Direc.add(this.x, this.y, direc)
		local tileid = GSMap.GetTileIndex(xy[0], xy[1])
		return tileid
	}

	function Neighbors(direcs) {
		local out = []
		foreach (direc in direcs) {
			out.append(this.Neighbor(direc))
		}
		return out
	}

	function NeighborsByRadius(n) {
		local out = Set(this.Neighbors(Direc.All))
		while (n > 1) {
			foreach (id in out) {
				local t = Tile(id)
				foreach(id2 in t.Neighbors(Direc.All))
					out.add(id2)
			}
			n--
		}
		return out
	}

	function canFoundCity() {
	}
}

class TileGroup {
	_tiles = null

	constructor(tiles) {
		this._tiles = Set(tiles)
	}

	LevelLand() {
	}

	ClearLand() {
	}

	//check out isBuildableRectangle
}
