dofile("CoreUtils.nut")
dofile("data.nut")

pprint <- CoreUtils.pprint
Array <- CoreUtils.Array

class CityData {

	static KEYS = {
		STATE=0,
		NAME=1,
		POPULATION=2,
		LATITUDE=3,
		LONGITUDE=4,
		ELEVATION=5,// not used yet, null
		BY_RIVER=6,// not used yet, null
		BY_SEA=7,// not used yet, null
		BIN_RANK=8, //FIXME we should null this not append it
	}

	data = null

	constructor(data=null) {
		if (data==null)
			data = GetCities().slice(0)
		this.data = data
	}

	function GetState(row) { return row[this.KEYS.STATE] }
	function GetName(row) { return row[this.KEYS.NAME] }
	function GetPopulation(row) { return row[this.KEYS.POPULATION] }
	function GetLatitude(row) { return row[this.KEYS.LATITUDE] }
	function GetLongitude(row) { return row[this.KEYS.LONGITUDE] }
	function GetCoord(row) { 
		return [ row[this.KEYS.LATITUDE], row[this.KEYS.LONGITUDE] ]
	}

	function ByColumn(key) { return Array.column(this.data, key) }

	function Select(limit=null) { //TODO
		local data
		if (limit != null)
			data = this.data.slice(0, limit)
		else
			data = this.data.slice(0)
		return CityData(data)
	}

	function ToCoords() {
		local coords = []
		foreach (city in this.data) {
			coords.append([ 
				city[this.KEYS.LATITUDE],
				city[this.KEYS.LONGITUDE]
			])
		}
		return coords
	}

	static function _test() {
		local data = CityData()
		data = data.Select(5)
		foreach (row in data.data) {
			print(data.GetState(row))
			print(" " + data.GetName(row))
			print(" " + data.GetPopulation(row))
			print(" " + data.GetLatitude(row))
			print(" " + data.GetLongitude(row))
			pprint(data.GetCoord(row))
		}
		local coords = data.ToCoords()
		foreach (coord in coords) {
			pprint(coord)
		}
		pprint(data.ByColumn(data.KEYS.STATE))
	}

}
//CityData._test()

