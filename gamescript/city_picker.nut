
foreach(k in CoreUtils.rootkeys(this))
	CoreUtils.pprint(k)

dofile("city_data.nut")
dofile("city_names.nut")
dofile("city_map.nut")

class CityPicker {

	map_size = null
	names = null
	data = null
	map = null
	selection = null

	constructor(map_size) { // should be [width, height]
		this.data = CityData()
		this.names = CityNames()
		this.map_size = map_size
	} 

	function Select(limit) {
		this.selection = this.data.Select(limit)
		local coords = this.selection.toCoords()
		this.map = CityMap(coords, this.map_size)

	}
	//names.PickNames(selection, map_bins)
	//pprint(map.toXY(coord))
}

