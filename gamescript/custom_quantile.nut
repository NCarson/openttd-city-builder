
function CustomQuantile(arr, binEdges) {

	local last = null
	foreach(val in arr) {
		if (last != null && last > val)
			throw "array must be sorted in ascending order"
		last = val
	}
		
	// Calculate the quantile boundaries based on the custom bin edges
	local nBins = binEdges.len() - 1
	local binIndices = [0]
	for (local i = 1; i <= nBins; i++) {
		binIndices.push(round(binEdges[i] * arr.len()))
	}

	// Assign each data point to its corresponding bin based on the quantile boundaries
	local bins = []
	local dataIndex = 0
	for (local i = 1; i <= nBins; i++) {
		local binEnd = binIndices[i] - 1
		while (dataIndex <= binEnd) {
			bins.push(i)
			dataIndex++
		}
	}
	// Return the resulting bins
	return bins
}
