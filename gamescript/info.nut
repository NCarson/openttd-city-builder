class FMainClass extends GSInfo {
	function GetAuthor()		{ return "Nate Carson"; }
	function GetName()			{ return "Deluxe Town Names"; }
	function GetDescription() 	{ return "Intelligently create town names *after* world gen"; }
	function GetVersion()		{ return 10; }
	function GetDate()			{ return "2023-03-29"; }
	function CreateInstance()	{ return "TownNames"; }
	function GetShortName()		{ return "DTWN"; } // Replace this with your own unique 4 letter string
	function GetAPIVersion()	{ return "1.2"; }
	//function GetURL()			{ return ""; } TODO

	function GetSettings() {
		AddSetting({name = "log_level", 
			description = "Debug: Log level (higher = print more)", 
			easy_value = 3, 
			medium_value = 3, 
			hard_value = 3, 
			custom_value = 3, 
			flags = CONFIG_INGAME, 
			min_value = 1, 
			max_value = 3
		});
		AddLabels("log_level", 
			{_1 = "1: Info", 
			_2 = "2: Verbose", 
			_3 = "3: Debug" } );
	}
}

RegisterGS(FMainClass());
