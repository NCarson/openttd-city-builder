// Remember to set dependencies in the bananas web manager for all libraries
// that you use. This way users will automatically get all libraries of the
// version you selected when they download your Game Script.

dofile <- require
import("util.CoreUtils", "CoreUtils", 10)

require("logger.nut");
require("city_picker.nut");

logger <- Logger("main.nut")

function listtoarray(list) {
	local arr = []
	foreach(item in list) {
		arr.push(item)
	}
	return arr
}

class TownNames extends GSController 
{
	_loaded_data = null
	_init_done = null

	/*
	 * This method is called when your GS is constructed.
	 * It is recommended to only do basic initialization of member variables
	 * here.
	 * Many API functions are unavailable from the constructor. Instead do
	 * or call most of your initialization code from TownNames::Init.
	 */
	constructor()
	{

		GSLog.Info("constructor()")
		this._init_done = false;
		this._loaded_data = null;
		logger.Log(Logger.DEBUG, "TownNames.constructor", "testing ...")
	}
}

/*
 * This method is called by OpenTTD after the constructor, and after calling
 * Load() in case the game was loaded from a save game. You should never
 * return back from this method. (if you do, the script will crash)
 *
 * Start() contains of two main parts. First initialization (which is
 * located in Init), and then the main loop.
 */
function TownNames::Start()
{
	logger.Log(Logger.DEBUG, "TownNames.Start", "testing ...")
	// Some OpenTTD versions are affected by a bug where all API methods
	// that create things in the game world during world generation will
	// return object id 0 even if the created object has a different ID. 
	// In that case, the easiest workaround is to delay Init until the 
	// game has started.
	//if (Helper.HasWorldGenBug()) GSController.Sleep(1);

	this.Init();

	// Wait for the game to start (or more correctly, tell OpenTTD to not
	// execute our GS further in world generation)
	GSController.Sleep(1);

	// Game has now started and if it is a single player game,
	// company 0 exist and is the human company.

	// Main Game Script loop
	local last_loop_date = GSDate.GetCurrentDate();
	while (true) {
		local loop_start_tick = GSController.GetTick();

		// Handle incoming messages from OpenTTD
		this.HandleEvents();

		// Reached new year/month?
		local current_date = GSDate.GetCurrentDate();
		if (last_loop_date != null) {
			local year = GSDate.GetYear(current_date);
			local month = GSDate.GetMonth(current_date);
			if (year != GSDate.GetYear(last_loop_date)) {
				this.EndOfYear();
			}
			if (month != GSDate.GetMonth(last_loop_date)) {
				this.EndOfMonth();
			}
		}
		last_loop_date = current_date;
	
		// Loop with a frequency of five days
		local ticks_used = GSController.GetTick() - loop_start_tick;
		GSController.Sleep(max(1, 5 * 74 - ticks_used));
	}
}

/*
 * This method is called during the initialization of your Game Script.
 * As long as you never call Sleep() and the user got a new enough OpenTTD
 * version, all initialization happens while the world generation screen
 * is shown. This means that even in single player, company 0 doesn't yet
 * exist. The benefit of doing initialization in world gen is that commands
 * that alter the game world are much cheaper before the game starts.
 */
function TownNames::Init()
{
	logger.Log(Logger.DEBUG, "TownNames.Init", "testing ...")
	if (this._loaded_data != null) {
		// Copy loaded data from this._loaded_data to this.*
		// or do whatever you like with the loaded data
	} else {
		// construct goals etc.
	}

	// Indicate that all data structures has been initialized/restored.
	this._init_done = true;
	this._loaded_data = null; // the loaded data has no more use now after that _init_done is true.

	RenameCities()
	//CreateMap()
}

/*
 * This method handles incoming events from OpenTTD.
 */
function TownNames::HandleEvents()
{

	logger.Log(Logger.DEBUG, "TownNames.HandleEvents", "testing ...")

	if(GSEventController.IsEventWaiting()) {
		local ev = GSEventController.GetNextEvent();
		if (ev == null) return;

		local ev_type = ev.GetEventType();
		switch (ev_type) {
			case GSEvent.ET_COMPANY_NEW: {
				logger.Log(Logger.DEBUG, "TownNames.HandleEvents", "new company ...")

				local company_event = GSEventCompanyNew.Convert(ev);
				local company_id = company_event.GetCompanyID();

				// Here you can welcome the new company
				//Story.ShowMessage(company_id, GSText(GSText.STR_WELCOME, company_id));
				//GSLog.Warning("WARN: I am game script in the event loop")
				//GSLog.Error("Error: I am game script in the event loop")
				break;
			}
			// other events ...
		}
	}
}

/*
 * Called by our main loop when a new month has been reached.
 */
function TownNames::EndOfMonth()
{
}

/*
 * Called by our main loop when a new year has been reached.
 */
function TownNames::EndOfYear()
{
}

/*
 * This method is called by OpenTTD when an (auto)-save occurs. You should
 * return a table which can contain nested tables, arrays of integers,
 * strings and booleans. Null values can also be stored. Class instances and
 * floating point values cannot be stored by OpenTTD.
 */
function TownNames::Save()
{
	logger.Log(Logger.DEBUG, "TownNames.Save", "testing ...")

	// In case (auto-)save happens before we have initialized all data,
	// save the raw _loaded_data if available or an empty table.
	if (!this._init_done) {
		return this._loaded_data != null ? this._loaded_data : {};
	}

	return { 
		some_data = null,
		//some_other_data = this._some_variable,
	};
}

/*
 * When a game is loaded, OpenTTD will call this method and pass you the
 * table that you sent to OpenTTD in Save().
 */
function TownNames::Load(version, tbl)
{
	logger.Log(Logger.DEBUG, "TownNames.Load", "testing ...")

	// Store a copy of the table from the save game
	// but do not process the loaded data yet. Wait with that to Init
	// so that OpenTTD doesn't kick us for taking too long to load.
	this._loaded_data = {}
   	foreach(key, val in tbl) {
		this._loaded_data.rawset(key, val);
	}
}

function RenameCities() {

	// get all the towns on the map
	local townlist = GSTownList()
	// sort list by population
	townlist.Valuate(GSTown.GetPopulation)
	// set up ranked bins by populatin
	local cutoffs = CityNames.BIN_CUTOFFS
	local arr = listtoarray(townlist)
	local map_bins = CustomQuantile(arr, cutoffs) // XXX CustomQuantile will always give back array in sorted ascending order
	map_bins.reverse()

	local map_size = [255, 255] // FIXME
	local picker = CityPicker()
	// select which cities to use TODO
	local selection = picker.data.Select()
	// pick our new names
	local new_names = picker.names.PickNames(selection, map_bins)

	local i = 0
	foreach (idx, pop in townlist) {
		local old = GSTown.GetName(idx)
		local new = new_names[i]
		GSTown.SetName(idx, new)
		logger.Log(Logger.DEBUG, "RenameCities", "rename " + old + " to " + new)
		local tile = GSTown.GetLocation(idx)
		println("Tile ID: " + tile)
		println("Town ID: " + GSTile.GetClosestTown(tile))
		i++
	}
}


function CreateMap() {
	local townlist = GSTownList()
	foreach (id in townlist)
		logger.Log(Logger.DEBUG, "CreateMap", "town id: " + id)

	local data = CityData()
	local selection = data.SelectCities(10)
	local coords = data.toCoords(selection)
	local map = CityMap(coords, [256, 256])

	foreach (row in selection) {
		local city_datum = CityDatum(data, map, row)

		//tileid, size enum, iscity, road layout enum, name`

		/* 
		see: CmdFoundTown and TownCanBePlaced here in twn_cmd.cpp

		game scripts have relaxed limits that players dont have:
		   - founding towns allowed in settings
		   - the town cannot be of large size
		   - road layout must match settings

		but not this one:
		   - the town cannot be randomly placed

		A suitable town site will:
		   1. not be less than 12 tiles from edge of map
		   2. not be less than 20 tiles from another town (Manhattan Distance)
		   3. be a flat tile
		   4. be clear except for trees
		 */

		city_datum.CanFound() // test this before we believe it
		local success = GSTown.FoundTown(
			city_datum.tileid, 
			GSTown.TOWN_SIZE_MEDIUM, 
			false, // is_city
			GSTown.ROAD_LAYOUT_ORIGINAL, 
			city_datum.name)
		if (success)
			logger.Log(Logger.DEBUG, "CreateMap", "founded town: " + city_datum.tostring())
		else
			logger.Log(Logger.DEBUG, "CreateMap", "founding town failed: " + city_datum.tostring())
	}
}

class CityDatum {
	// close methods based around wheather openttd will let us found. see town_cmd.cpp

	name = null
	xy = null
	population = null
	tileid = null

	constructor(data, map, row) {
		local coord = data.getCoord(row)
		this.xy = map.toXY(coord)
		printarray("xy: " + this.xy)
		this.tileid = GSMap.GetTileIndex(this.xy[0], this.xy[1])
		this.name = data.getName(row)
		this.population = data.getPopulation(row)

		if (this.CloseToEdge())
			logger.Error("CityDatum.constructor", 
				"city to close to edge: " + this.tostring())
	}

	function tostring() {
		return name + " (" + this.xy[0] + "," + this.xy[1] + ")"
	}

	function CloseToCity() {
		local townid = GSTile.GetClosestTown(this.tileid)
		return GSTile.IsWithinTownInfluence(this.tileid, townid)
	}

	function IsFlat() {
		return GSTile.GetSlope(this.tileid) == GSTile.SLOPE_FLAT
	}

	function CloseToEdge() {
		return GSMap.DistanceFromEdge(this.tileid) < 12
	}

	function IsBuildable() { //FIXME check conditions again in town command
		return GSTile.IsBuildable(this.tileid)
	}

	function CanFound() {
		if (this.CloseToEdge()) {
			logger.Debug("CityDatum.CanFound", this.tostring() + " is too close the edge to found")
			return false
		}
		if (this.CloseToCity()) {
			logger.Debug("CityDatum.CanFound", this.tostring() + " is too close to another city to found")
			return false
		}
		if (!this.IsFlat()) {
			logger.Debug("CityDatum.CanFound", this.tostring() + " is not on a flat tile to found")
			return false
		}
		if (!this.IsBuildable()) {//FIXME check if this includes trees
			logger.Debug("CityDatum.CanFound", this.tostring() + " is not buildable")
			return false
		}
		return true
	}
}


