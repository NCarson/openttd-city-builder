dofile("CoreUtils.nut") // we need a copy of core utils for command line access

Array <- CoreUtils.Array
Math <- CoreUtils.Math
pprint <- CoreUtils.pprint

class BoundingBox {
	min_lat = null
	max_lat = null
	min_lng = null
	max_lng = null
	padding = 12

	constructor(coords) {
		this.min_lat = Array.min(Array.column(coords, 0))
		this.max_lat = Array.max(Array.column(coords, 0))
		this.min_lng = Array.min(Array.column(coords, 1))
		this.max_lng = Array.max(Array.column(coords, 1))

		// add padding so cities are not right on edge

		// FIXME this will have wraparound issues in certain locations
		local map_size = 255 //FIXME
		local lat_pad = (this.max_lat - this.min_lat) / map_size * this.padding * 2
		local lng_pad = (this.max_lng - this.min_lng) / map_size * this.padding * 2
		this.min_lat -= lat_pad
		this.max_lat += lat_pad
		this.min_lng -= lng_pad
		this.max_lng += lng_pad
    }

	function contains(coord) {
        // Check if GPS coordinate is within bounding box
        if (coord[0] < this.min_lat 
			|| coord[0] > this.max_lat 
			|| coord[1] < this.min_lng 
			|| coord[1] > this.max_lng) {
            return false
        }
		return true
	}
}

class CityMap {
	
	bounding_box = null
	map_size = null

	constructor(coords, map_size) {
		this.map_size = map_size
		this.bounding_box = BoundingBox(coords)
	}

    function toXY(gps_coord) {
		//local lat_off = 0.1 //FIXME put this in constructor somewhere
		//local lng_off = 0.4
		local lat_off = 0.0
		local lng_off = 0.0

        // Check if GPS coordinate is within bounding box
		//if (!this.bounding_box.contains(gps_coord))
		//	return null
        
        // Convert GPS coordinates to x, y coordinates using the IV quadrant in relation to the origin
        local lat_diff = lat_off + gps_coord[0] - this.bounding_box.min_lat;
        local lng_diff = lng_off + gps_coord[1] - this.bounding_box.min_lng;
        local x = lng_diff * cos(Math.radians(this.bounding_box.min_lat));
        local y = lat_diff;
        
        // Scale x and y coordinates to map size and round to nearest integer tile index
        x = Math.round((x / (this.bounding_box.max_lng - this.bounding_box.min_lng)) * (this.map_size[0] - 1));
        y = Math.round((y / (this.bounding_box.max_lat - this.bounding_box.min_lat)) * (this.map_size[1] - 1));
		x = this.map_size[0] - x
		y = this.map_size[1] - y
        return [x, y];
    }

	static function _test() {

		pprint("--------------------")

		local cities = CityData()
		local selection = cities.SelectCities(5)
		local coords = cities.toCoords(selection)
		local map = CityMap(coords, [256, 256])
		foreach (i, coord in coords) {
			pprint("i:"+ i + " ", "    ")
			pprint(map.toXY(coord))
		}
	}
}

// XXX this has to be done in outer scope as it is lost if we do it in a function
try {
	require("city_data.nut")
} catch(err) {
	dofile("city_data.nut")
}
//CityMap._test()
